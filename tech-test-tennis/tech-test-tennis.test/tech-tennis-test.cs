﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using tech_test_tennis.game;
using tech_test_tennis.game.Interfaces;

namespace tech_test_tennis.test
{

    [TestClass]
    public class tech_tennis_test
    {
        private readonly IJogador _jogadorService;
        private readonly IPlacar _placarService;

        public tech_tennis_test()
        {
            _jogadorService = new Jogador();
            _placarService = new Placar();
        }

        [TestMethod]
        public void IsConverterPontucao_Success() 
        {
            var expected = _placarService.ConverterPontucao(1);

            var actual = "15";

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsObter_Pontuacao_Success() 
        {
            var expected = _placarService.ObterPontuacao(new int[2] { 0, 1 }, "Guga", "Agassi");

            var actual = new Placar
            {
                Resultado = string.Empty,
                FimDeJogo = false
            };

            Assert.AreEqual(expected.FimDeJogo, actual.FimDeJogo);
        }


        [TestMethod]
        public void IsCadastrar_Jogadores_Success()
        {
            var result = _jogadorService.CadastrarJogadores("1");

            Assert.IsTrue(result.Count > 1, "Sucesso ao buscar mais de um jogador.");
        }


        [TestMethod]
        public void IsPontuou_Success() 
        {
            var result = _jogadorService.Pontuou();

            Assert.IsNotNull(result);
        }
    }
}
