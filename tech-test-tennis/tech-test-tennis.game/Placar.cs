﻿using System;
using System.Collections.Generic;
using System.Text;
using tech_test_tennis.game.Interfaces;

namespace tech_test_tennis.game
{
    public class Placar : IPlacar
    {
        public string Resultado { get; set; }
        public bool FimDeJogo { get; set; } = false;

        public Placar ObterPontuacao(int[] points, string jogadorUm, string jogadorDois) 
        {
            if (points[0] >= 3 && points[1] >= 3 && Math.Abs(points[0] - points[1]) < 2) 
            {
                if (points[0] == points[1])
                {
                    return new Placar
                    {
                        Resultado = "deuce"
                    };
                }

                if (points[0] > points[1])
                {
                    return new Placar
                    {
                        Resultado = "advantage" + jogadorUm
                    };
                }
                else
                {
                    return new Placar
                    {
                        Resultado = "advantage" + jogadorDois
                    };                    
                }            
            }

            if (points[0] < 4 && points[1] < 4) 
            {
                return new Placar
                {
                    Resultado = jogadorUm + " " + ConverterPontucao(points[0]) + " - " + ConverterPontucao(points[1]) + " " + jogadorDois
                };
            }

            FimDeJogo = true;

            if (points[0] >= 4) 
            {
                return new Placar
                {
                    Resultado = ConverterPontucao(points[0]) + " " + jogadorUm
                };
            }           

            return new Placar
            {
                Resultado = ConverterPontucao(points[1]) + " " + jogadorDois
            };
        }


        public string ConverterPontucao(int points)
        {
            switch (points)
            {
                case 3:
                    return "40";
                case 2:
                    return "30";
                case 1:
                    return "15";
                case 0:
                    return "0";
                default:
                    return "Campeão";
            }
        }
    }
}
