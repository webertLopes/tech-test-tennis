﻿using System;
using System.Collections.Generic;
using System.Text;
using tech_test_tennis.game.Interfaces;

namespace tech_test_tennis.game
{
    public class Jogador : IJogador
    {
        public int Id { get; set; }
        public string Nome { get; set; }        
        public Jogador()
        {

        }
        public Jogador(int id, string nome)
        {
            this.Id = id;
            this.Nome = nome;
        }

        public List<Jogador> CadastrarJogadores(string jogadorEscolhido) 
        {
            List<Jogador> jogador = new List<Jogador>();

            if (jogadorEscolhido.Equals("1"))
            {
                jogador.Add(new Jogador { Id = 1, Nome = "Gustavo Kierten" });
                jogador.Add(new Jogador { Id = 2, Nome = "Andre Agassi" });
            }
            else
            {
                jogador.Add(new Jogador { Id = 2, Nome = "Gustavo Kierten" });
                jogador.Add(new Jogador { Id = 1, Nome = "Andre Agassi" });
            }

            return jogador;
        }

        public bool Pontuou() 
        {
            Random gen = new Random();
            int prob = gen.Next(100);
            return prob <= 20;
        }

    }
}
