﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tech_test_tennis.game.Interfaces
{
    public interface IPlacar
    {
        Placar ObterPontuacao(int[] points, string jogadorUm, string jogadorDois);
        string ConverterPontucao(int points);
        public string Resultado { get; set; }
        public bool FimDeJogo { get; set; }
    }
}
