﻿using System;
using System.Collections.Generic;
using System.Text;

namespace tech_test_tennis.game.Interfaces
{
    public interface IJogador
    {
        List<Jogador> CadastrarJogadores(string jogadorEscolhido);
        bool Pontuou();
    }
}
