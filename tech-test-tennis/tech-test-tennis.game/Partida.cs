﻿using System;
using System.Collections.Generic;
using System.Text;
using tech_test_tennis.game.Interfaces;

namespace tech_test_tennis.game
{
    public class Partida 
    {
        public List<Jogador> Jogador { get; set; }
        public Placar Placar { get; set; }

        public Partida()
        {
            this.Jogador = new List<Jogador>();
        }
    }
}
