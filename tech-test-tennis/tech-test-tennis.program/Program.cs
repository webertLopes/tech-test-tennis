﻿using System;
using System.Collections.Generic;
using System.Linq;
using tech_test_tennis.game;
using tech_test_tennis.game.Interfaces;

namespace tech_test_tennis.program
{
    public class Program
    {
        public static void Main()
        {
            MenuJogo();

            var opcao = Console.ReadKey();

            IJogador usuario = new Jogador();

            Partida partida = new Partida();

            List<Jogador> jogadores;

            switch (opcao.KeyChar)
            {
                case '1':
                    Console.WriteLine("Jogador escolhido - Gustavo Kierten");
                    jogadores = usuario.CadastrarJogadores("1");
                    break;
                case '2':
                    Console.WriteLine("Jogador escolhido - Andre Agassi");
                    jogadores = usuario.CadastrarJogadores("2");
                    break;
                default:
                    Console.WriteLine("Nenhum jogador foi escolhido - jogo cancelado.");
                    return;
            }

            partida.Jogador.AddRange(jogadores);          
            
            PainelJogo(partida);

            Console.WriteLine("Fim de Jogo !");
        }

        private static void MenuJogo()
        {
            Console.Clear();
            Console.WriteLine("Torneio de Winbledom");
            Console.WriteLine("Escolha o seu personagem - ele irá iniciar o jogo :");
            Console.WriteLine();
            Console.WriteLine("1 - Gustavo Kierten");
            Console.WriteLine("2 - Andre Agassi");
        }


        private static void PainelJogo(Partida partida)
        {
            int countPlayerUm = 0;
            int countPlayerDois = 0;

            IPlacar placar = new Placar();
            string resultado = string.Empty;
            string jogadorDaVez = partida.Jogador.OrderBy(x => x.Id).First().Nome;

            do
            {
                Console.WriteLine("Digite Enter para fazer a jogada: " + jogadorDaVez);
                Console.ReadKey();

                if (partida.Jogador.First().Pontuou())
                {
                    countPlayerUm++;
                    Console.WriteLine("Ponto para : " + partida.Jogador.OrderBy(x => x.Id).First().Nome);
                    jogadorDaVez = partida.Jogador.OrderBy(x => x.Id).First().Nome;
                    resultado = placar.ObterPontuacao(new int[2] { countPlayerUm, countPlayerDois }, partida.Jogador.OrderBy(x => x.Id).First().Nome, partida.Jogador.OrderBy(x => x.Id).Last().Nome).Resultado;
                    Console.WriteLine(resultado);
                }
                else
                {
                    countPlayerDois++;
                    Console.WriteLine("Ponto para : " + partida.Jogador.OrderBy(x => x.Id).Last().Nome);
                    jogadorDaVez = partida.Jogador.OrderBy(x => x.Id).Last().Nome;
                    resultado = placar.ObterPontuacao(new int[2] { countPlayerUm, countPlayerDois }, partida.Jogador.OrderBy(x => x.Id).First().Nome, partida.Jogador.OrderBy(x => x.Id).Last().Nome).Resultado;
                    Console.WriteLine(resultado);
                }


            } while (!placar.FimDeJogo);
        }


    }
}
